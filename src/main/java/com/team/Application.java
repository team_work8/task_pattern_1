package com.team;

import com.team.view.View;

public class Application {

    public static void main(String[] args){
        new View().interact();
    }
}
