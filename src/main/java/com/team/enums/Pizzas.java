package com.team.enums;

public enum Pizzas {
    CHEESE,
    VEGGIE,
    CLAM,
    PEPPERONI
}
