package com.team.factory;

import com.team.enums.Pizzas;
import com.team.pizza.Pizza;

public interface Factory {

    Pizza choosePizza(Pizzas pizza);

    void makePizza(Pizzas pizza);
}
