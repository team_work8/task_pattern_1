package com.team.factory;

import com.team.pizza.Cheese;
import com.team.pizza.Clam;
import com.team.pizza.Pepperoni;
import com.team.pizza.Veggie;
import com.team.enums.Pizzas;
import com.team.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class KyivFactory implements Factory {

    private static Logger logger = LogManager.getLogger();

    @Override
    public final Pizza choosePizza(final Pizzas pizza) {
        if (pizza.equals(Pizzas.CHEESE)) {
            return new Cheese(25, "Garlic", "Species");
        } else if (pizza.equals(Pizzas.CLAM)) {
            return new Clam(25, "Garlic");
        } else if (pizza.equals(Pizzas.PEPPERONI)) {
            return new Pepperoni(25, "Sausages", "Bacon");
        } else if (pizza.equals(Pizzas.VEGGIE)) {
            return new Veggie(25, "Cherry tomatoes", "Green pepper");
        }
        return null;
    }

    @Override
    public final void makePizza(final Pizzas pizzas) {
        Pizza pizza = choosePizza(pizzas);
        assert pizza != null;
        logger.info("Your choice - " + pizzas.name() + " pizza from Kyiv!");
        pizza.info();
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        logger.info("Your " + pizzas.name() + " pizza is ready for delivery\n");
    }
}
