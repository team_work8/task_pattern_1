package com.team.factory;

import com.team.pizza.Cheese;
import com.team.pizza.Clam;
import com.team.pizza.Pepperoni;
import com.team.pizza.Veggie;
import com.team.enums.Pizzas;
import com.team.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LvivFactory implements Factory {

    private static Logger logger = LogManager.getLogger();

    public final Pizza choosePizza(final Pizzas pizza) {
        if (pizza.equals(Pizzas.CHEESE)) {
            return new Cheese(40, "Gorgonzola", "Additional Cheddar");
        } else if (pizza.equals(Pizzas.CLAM)) {
            return new Clam(40, "Seasonings", "Eggs");
        } else if (pizza.equals(Pizzas.PEPPERONI)) {
            return new Pepperoni(40, "Chicken", "Tomatoes");
        } else if (pizza.equals(Pizzas.VEGGIE)) {
            return new Veggie(40, "Carrot", "Onion");
        }
        return null;
    }

    public final void makePizza(final Pizzas pizzas) {

        Pizza pizza = choosePizza(pizzas);
        assert pizza != null;
        logger.info("Your choice - " + pizzas.name() + " pizza from Lviv!");
        pizza.info();
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        logger.info("Your " + pizzas.name() + " pizza is ready for delivery\n");
    }
}
