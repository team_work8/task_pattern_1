package com.team.factory;

import com.team.pizza.Cheese;
import com.team.pizza.Clam;
import com.team.pizza.Pepperoni;
import com.team.pizza.Veggie;
import com.team.enums.Pizzas;
import com.team.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DniproFactory implements Factory {

    private static Logger logger = LogManager.getLogger();

    @Override
    public final Pizza choosePizza(final Pizzas pizza) {
        if (pizza.equals(Pizzas.CHEESE)) {
            return new Cheese(30, "Chicken", "Tomatoes");
        } else if (pizza.equals(Pizzas.CLAM)) {
            return new Clam(30, "Corn", "Celery");
        } else if (pizza.equals(Pizzas.PEPPERONI)) {
            return new Pepperoni(30, "Beef", "Hot pepper");
        } else if (pizza.equals(Pizzas.VEGGIE)) {
            return new Veggie(30, "Salad", "Chicken eggs");
        }
        return null;
    }

    @Override
    public final void makePizza(final Pizzas pizzas) {
        Pizza pizza = choosePizza(pizzas);
        assert pizza != null;
        logger.info("Your choice - " + pizzas.name() + " pizza from Dnipro!");
        pizza.info();
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        logger.info("Your " + pizzas.name() + " pizza is ready for delivery\n");
    }

}