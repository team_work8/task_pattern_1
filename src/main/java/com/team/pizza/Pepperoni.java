package com.team.pizza;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Pepperoni implements Pizza {

    private Logger logger = LogManager.getLogger();
    private String dough = "thick";
    private String sauce = "Tomato";
    private int radius = 30;
    private List<String> toppings = new LinkedList<>(Arrays.asList("Pork", "Sweet paprika", "Mozzarella"));

    public Pepperoni(final int radius, final String... ingredients) {
        toppings.addAll(Arrays.asList(ingredients));
        this.radius = radius;
    }

    public final void prepare() {
        logger.info("Your Pepperoni pizza is being prepared");
    }

    public final void bake() {
        logger.info("Your Pepperoni pizza is being baked");
    }

    public final void cut() {
        logger.info("Your Pepperoni pizza is being cut");
    }

    public final void box() {
        logger.info("Your Pepperoni pizza is being boxed");
    }

    public final String getDough() {
        return dough;
    }

    public final void setDough(final String dough) {
        this.dough = dough;
    }

    public final String getSauce() {
        return sauce;
    }

    public final void setSauce(final String sauce) {
        this.sauce = sauce;
    }

    public final List<String> getToppings() {
        return toppings;
    }

    public final void setToppings(final List<String> toppings) {
        this.toppings = toppings;
    }

    public final int getRadius() {
        return radius;
    }

    public final void setRadius(final int radius) {
        this.radius = radius;
    }

    public final void info() {
        logger.info("Pepperoni{"
                + "radius" + radius + '\''
                + ", dough='" + dough + '\''
                + ", sauce='" + sauce + '\''
                + ", toppings=" + toppings + '}');
    }
}
