package com.team.pizza;

public interface Pizza {

    void prepare();

    void bake();

    void cut();

    void box();

    void info();
}
