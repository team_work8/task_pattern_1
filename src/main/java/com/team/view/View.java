package com.team.view;

import com.team.enums.Pizzas;
import com.team.factory.DniproFactory;
import com.team.factory.Factory;
import com.team.factory.KyivFactory;
import com.team.factory.LvivFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {

    private static Logger logger = LogManager.getLogger();
    private static Map<String,String>cities;
    private static Map<String,String>pizzas;
    private static Map<String,Executable>cMethods;
    private static Map<String,Pizzas>pMethods;
    private static Scanner scanner = new Scanner(System.in);
    private static Factory factory;

    public View(){
        cities = new LinkedHashMap<>();
        cities.put("1","1 - Kyiv");
        cities.put("2","2 - Lviv");
        cities.put("3","3 - Dnipro");
        cities.put("Q","Q - Quit");

        pizzas = new LinkedHashMap<>();
        pizzas.put("1","1 - Cheese");
        pizzas.put("2","2 - Clam");
        pizzas.put("3","3 - Pepperoni");
        pizzas.put("4","4 - Veggie");
        pizzas.put("Q","Q - Quit");

        cMethods = new LinkedHashMap<>();
        cMethods.put("1",this::cFirst);
        cMethods.put("2",this::cSecond);
        cMethods.put("3",this::cThird);
        cMethods.put("Q",this::exit);

        pMethods = new LinkedHashMap<>();
        pMethods.put("1",Pizzas.CHEESE);
        pMethods.put("2",Pizzas.CLAM);
        pMethods.put("3",Pizzas.PEPPERONI);
        pMethods.put("4",Pizzas.VEGGIE);
    }

    private void cFirst(){
        factory = new KyivFactory();
    }

    private void cSecond(){
        factory = new LvivFactory();
    }

    private void cThird(){
        factory = new DniproFactory();
    }

    private void exit(){
        logger.trace("Bye!");
        System.exit(0);
    }

    private void showCities(){
        logger.trace("~~~~~~~CITY~~~~~~~");
        for (String s:cities.values()) {
            logger.trace(s);
        }
        logger.trace("~~~~~~~CITY~~~~~~~");
    }

    private void showPizzas(){
        logger.trace("~~~~~~~PIZZA~~~~~~~");
        for (String s:pizzas.values()) {
            logger.trace(s);
        }
        logger.trace("~~~~~~~PIZZA~~~~~~~");
    }

    public void interact(){
        String key1,key2;
        do{
            showCities();
            logger.trace("Choose city:");
            logger.trace("---> ");
            key1 = scanner.nextLine().toUpperCase();
            cMethods.get(key1).execute();

            do {
                showPizzas();
                logger.trace("Choose pizza:");
                logger.trace("---> ");
                key2 = scanner.nextLine().toUpperCase();
                if(key2.equals("Q"))break;
                factory.makePizza(pMethods.get(key2));

            }while (!key2.equals("Q"));

        }while (!key1.equals("Q"));
    }
}
